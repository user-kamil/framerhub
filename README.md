# Linting

* A set of strict linting rules (based on the [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript))
  * `lint` script
* Encourage automatic code formatting
  * `format` script
* [Yarn](https://yarnpkg.com) for package management
* Use [EditorConfig](http://editorconfig.org) to maintain consistent coding styles between different editors and IDEs
* Integration with [Visual Studio Code](https://code.visualstudio.com)
  * Pre-configured auto-formatting on file save
