import { injectGlobal } from 'styled-components';

/* eslint-disable no-unused-expressions */

injectGlobal`
  * {
    box-sizing: border-box;
    -webkit-overflow-scrolling: touch;
  }
  html {
    overflow-y: scroll;
  }
  body {
    color: #fafafa;
    background: #121212;
  }

  *::-webkit-scrollbar {
    width: 6px;
  }
  *::-webkit-scrollbar-track {
    background: #eee;
  }
  *::-webkit-scrollbar-thumb {
    background-color: #ccc;
  }
  [data-whatintent="mouse"] *:focus {
    outline: none;
  }
`;

/* eslint-enable no-unused-expressions */

const theme = {
  space: [0, 4, 8, 12, 16, 20, 24, 32, 48, 64, 128, 256, 512],
  breakpoints: [22, 32, 48, 64, 80, 90, 120],
  fontSizes: [8, 10, 12, 14, 16, 20, 24, 32, 48, 64, 72, 96],
};

export default theme;
