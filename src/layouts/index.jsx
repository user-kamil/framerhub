import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { Provider, Container, Heading } from 'rebass';
import theme from '../styles/theme';
import Header from '../components/Header/Index';

const IndexLayout = ({ children }) => (
  <Provider theme={theme}>
    <Helmet
      title="Framer hub"
      meta={[{ name: 'description', content: 'Sample' }, { name: 'keywords', content: 'sample, something' }]}
    />
    <Container>
      <Header />
      <Heading width={[1 / 2]} center mx="auto">
        Bets place to start your FRAMER journey
      </Heading>
      {children()}
    </Container>
  </Provider>
);

IndexLayout.propTypes = {
  children: PropTypes.func.isRequired,
};

export default IndexLayout;
