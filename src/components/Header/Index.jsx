import React from 'react';
import Link from 'gatsby-link';
import styled from 'styled-components';

const HeaderContainer = styled.div`
  margin: 0 auto;
  max-width: 960;
  padding: 1.45rem 1.0875rem;
`;

const HeaderLink = styled(Link)`
  font-size: 20px;
  color: white;
  font-weight: 700;
  letter-spacing: 1px;
  text-decoration: none;
`;

const Header = () => (
  <HeaderContainer>
    <HeaderLink to="/">framerhub</HeaderLink>
  </HeaderContainer>
);

export default Header;
